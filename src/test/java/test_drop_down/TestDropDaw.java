package test_drop_down;

import baseTest.WebDriverTest;
import org.page_object.DropDawnPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestDropDaw extends WebDriverTest {
    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/dropdown");
    }

    @Test
    public void testForDrop(){
        new DropDawnPage(driver)
                .clickSelectDropOne()
                .clickSelectDropTwo()
                .chutesSelect();
    }
}
