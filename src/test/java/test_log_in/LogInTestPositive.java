package test_log_in;

import baseTest.WebDriverTest;
import org.openqa.selenium.Cookie;
import org.page_object.LoginFormPage;
import org.page_object.MainPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Set;

public class LogInTestPositive extends WebDriverTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;
    String userName = "tomsmith";
    String userPassword = "SuperSecretPassword!";
    String expectedText = "You logged into a secure area!";

    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
    }

    @Test
    public void logInTest() {
        mainPage.clickBtnPronPage("login");
        loginFormPage.login(userName, userPassword)
                .checkSuccessTooltip(expectedText);
        Set<Cookie> cookies = driver.manage().getCookies();
        for (Cookie k : cookies) {
            System.out.println(k.getName() + "\n" + k.getValue() + "\n" + k.getExpiry());
        }

        Cookie cookie = new Cookie("My Name", "My value");
        driver.manage().addCookie(cookie);

        cookies = driver.manage().getCookies();
        for (Cookie k : cookies) {
            System.out.println(k.getName() + "\n" + k.getValue() + "\n" + k.getExpiry());
        }
    }
}

