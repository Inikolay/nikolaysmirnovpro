package test_log_in;

import baseTest.WebDriverTest;
import org.openqa.selenium.Cookie;
import org.page_object.LoginFormPage;
import org.page_object.MainPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Set;

public class NegativeTestLogInNotUserName extends WebDriverTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;

    String userPassword = "SuperSecretPassword!";

    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
    }

    @Test
    public void logInTest(){
        mainPage.clickBtnPronPage("login");
        loginFormPage.clickBtnLogIn()
                .writeUserPassword(userPassword)
                .getMessageFromTooltip(false);
        }
    }

