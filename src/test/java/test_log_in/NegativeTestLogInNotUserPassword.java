package test_log_in;

import baseTest.WebDriverTest;
import org.page_object.LoginFormPage;
import org.page_object.MainPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NegativeTestLogInNotUserPassword extends WebDriverTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;

    String userName = "tomsmith";

    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
    }

    @Test
    public void logInTest(){
        mainPage.clickBtnPronPage("login");
        loginFormPage.clickBtnLogIn()
                .writeUserName(userName)
                .getMessageFromTooltip(false);
    }
}
