package baseTest;




import org.driver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


public class WebDriverTest {

    protected WebDriver driver = null;

    @BeforeTest
    public void setUpBrowser(){
        driver = WebDriverFactory.initDriver();
        driver.manage().window().maximize();
    }


    @AfterTest
    public void closeDriver(){
        if (driver !=null){
            driver.quit();
        }
    }

    public void openUrl(String url){
        driver.navigate().to(url);
    }
}
