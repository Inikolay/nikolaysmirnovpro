package test_alert;

import baseTest.WebDriverTest;
import enam_frome_page.AlertButton;
import org.page_object.AlertsPage;
import org.page_object.MainPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class AlertTest extends WebDriverTest {

    AlertsPage alertsPage;
    @BeforeMethod
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @Test
    public void alertTest() {
       new MainPage(driver).clickBtnPronPage("javascript_alerts");
       alertsPage.clickOnButton(AlertButton.ALERT.getTextOnButton());
       Assert.assertEquals(alertsPage.switchToAlertAndGetText(true), "I am a JS Alert");
       Assert.assertEquals(alertsPage.getResaltText(), "You successfully clicked an alert");
    }
}
