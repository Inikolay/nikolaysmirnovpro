package org.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.testng.Assert.assertTrue;

public class DropDawnPage extends BasePage{
    public DropDawnPage(WebDriver driver) {
        super(driver);
    }

    private final WebElement selectDropOne = (WebElement) By.xpath("//select[@id='dropdown']/option[@value='1']");
    private final WebElement selectDropTwo = (WebElement) By.xpath("//select[@id='dropdown']/option[@value='2']");

    public DropDawnPage clickSelectDropOne(){
        driver.findElement((By) selectDropOne).click();
        return this;
    }

    public DropDawnPage clickSelectDropTwo(){
        driver.findElement((By) selectDropTwo).click();
        return this;
    }

    public DropDawnPage chutesSelect(){
        Select selectDrop = new Select(driver.findElement(By.id("dropdown")));

        selectDrop.selectByVisibleText("Option 2");
        assertTrue(selectDropTwo.isSelected());

        selectDrop.selectByVisibleText("Option 1");
        assertTrue(selectDropOne.isSelected());
        return this;
    }
}
